import React, {useState, useEffect} from 'react';
import {View, Text, Button, TextInput, SafeAreaView} from 'react-native';
import {ApolloClient, HttpLink, InMemoryCache} from 'apollo-boost';
import {ApolloProvider, useMutation} from '@apollo/react-hooks';

import gql from 'graphql-tag';

const LOGIN = gql`
    mutation login($username: String!, $password: String!) {
        login(username: $username, password: $password) {
            token
        }
    }
`;

export const SignIn = () => {
    const [login] = useMutation(LOGIN);
    const [inputValue, setInputValue] = useState({
        username: '',
        password: '',
    });

    const [message, setMessage] = useState("");

    const handleSubmit = () => {
        login({variables: inputValue}).then(res => {
            console.log("RES", res)
            setMessage(res.data.login.token)
        }).catch(console.log);
    };
  return (
    <View>
        <TextInput
            style={{height: 40, borderColor: 'gray', borderWidth: 1}}
            onChangeText={text => setInputValue({...inputValue, username: text})}
            value={inputValue.username}
        />
        <TextInput
            style={{height: 40, borderColor: 'gray', borderWidth: 1}}
            onChangeText={text => setInputValue({...inputValue, password: text})}
            value={inputValue.password}
        />
        <Button onPress={() => handleSubmit()} title={"LOGIN"}/>
        <Text>{message}</Text>
    </View>
  );
};

export default SignIn
