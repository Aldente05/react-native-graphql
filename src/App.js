import React, {useState} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TextInput,
    Button,
} from 'react-native';

import {ApolloClient, HttpLink, InMemoryCache} from 'apollo-boost';
import {ApolloProvider, useMutation} from '@apollo/react-hooks';

import gql from 'graphql-tag';

const LOGIN = gql`
    mutation login($username: String, $password: String) {
        login(username: $username, password: $password) {
            token
        }
    }
`;

import SignIn from './views/Authentication/SignIn';
import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

// Create the client as outlined in the setup guide
const client = new ApolloClient({
    link: new HttpLink({
        uri: 'http://localhost:3200',
    }),
    cache: new InMemoryCache(),
});

const App: () => React$Node = () => {
    return (
        <>
            <ApolloProvider client={client}>
                <StatusBar barStyle="dark-content"/>
                <SafeAreaView>
                    <SignIn/>
                </SafeAreaView>
            </ApolloProvider>
        </>
    );
};
export default App;
